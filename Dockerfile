FROM ubuntu:20.04

RUN apt-get -y update && \
    apt-get -y dist-upgrade && \
    apt-get -y autoremove && \
    apt-get -y autoclean && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get -y install \
    build-essential \
    cmake \
    git \
    libboost-iostreams-dev \
    libboost-system-dev \
    libjemalloc-dev \
    libtbb-dev \
    libilmbase-dev \
    libopenexr-dev \
    zlib1g-dev
    

# c-blosc
RUN git clone https://github.com/Blosc/c-blosc.git -b v1.5.0 && \
    mkdir c-blosc/build && \
    cd c-blosc/build && \
    cmake .. && \
    make -j8 && \
    make install && \
    cd ../.. && \
    rm -rf c-blosc

# VDB
RUN git clone https://github.com/AcademySoftwareFoundation/openvdb.git && \
    mkdir openvdb/build && \
    cd openvdb/build && \
    cmake .. && \
    make -j8 && \
    make install && \
    cd ../../ && \
    rm -rf openvdb
